package com.hendisantika.bootstrap.bootstrap;

import com.hendisantika.bootstrap.bootstrap.persistence.model.Book;
import com.hendisantika.bootstrap.bootstrap.persistence.repo.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BootStrapApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootStrapApplication.class, args);
	}

    @Bean
    CommandLineRunner initData(BookRepository repository) {
        return args -> {
            repository.save(new Book("Belajar Java Mudah!", "Endy Muhardin"));
            repository.save(new Book("Belajar PHP Mudah!", "Uchiha Sasuke"));
            repository.save(new Book("Belajar Scala Mudah!", "Alex Sihombing"));
            repository.save(new Book("Belajar Kotlin Mudah!", "Deny Prasetyo"));
        };
    }
}
