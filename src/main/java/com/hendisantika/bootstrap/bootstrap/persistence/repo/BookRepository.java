package com.hendisantika.bootstrap.bootstrap.persistence.repo;

import com.hendisantika.bootstrap.bootstrap.persistence.model.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by hendisantika on 6/14/17.
 */
public interface BookRepository extends CrudRepository<Book, Long>{
    List<Book> findByTitle(String title);
}
